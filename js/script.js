console.log(document.getElementById("toggle_switch").checked)


const checkbox= document.getElementById("toggle_switch")

checkbox.addEventListener("change", function(){

    let plans = document.getElementsByClassName("pricing")

    if(checkbox.checked){
    
        plans[0].innerHTML = "19.99"
        plans[1].innerHTML = "24.99"
        plans[2].innerHTML = "39.99"
    }
    else{
        plans[0].innerHTML = "199.99"
        plans[1].innerHTML = "249.99"
        plans[2].innerHTML = "399.99"
    }
})
